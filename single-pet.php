<?php get_header();?>       
       <div class="center_content">
       		<div class="left_content">
        		<div class="crumb_nav">
            		<a href="<?php echo get_option('home');?>">Home &gt&gt</a> <?php wp_title('&raquo;', true, 'right');?>
           		 </div>
           		 <?php if (have_posts()) : the_post(); ?>
           				 <div class="title">
							<?php the_title(); ?>
						 </div>
        					<?php 
        						$custom_details = get_post_meta($post->ID, 'pet_desc', true);
  								$custom_price = get_post_meta($post->ID,'price',true);
  								$custom_color = get_post_meta($post->ID,'meta-color',true);
  								$custom_color_2 = get_post_meta($post->ID,'color-2',true);
  								$name=get_the_title();
  								$id=get_the_ID();
  								if (has_post_thumbnail( $post->ID )) :
             						 $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ));
            					endif;
  							?>
        				<div class="feat_prod_box_details">
            				<div class="prod_img"><a href="details.html"><img src="images/prod1.gif" alt="" title="" border="0" /></a> <br /><br />
              					 <a rel="lightbox" href='<?php the_post_thumbnail();?>'><?php if(has_post_thumbnail()) : the_post_thumbnail(); endif;?> <br/><img border="0" title="" alt="" src="<?php bloginfo('template_directory');?>/images/zoom.gif"></a>
              				</div>
	                		<div class="prod_det_box">
	                			<div class="box_top"></div>
	                   				<div class="box_center ">
	                   					 <p class="details" style="font-size:16px;">Details</p>
	                   						 <div style="color:black;"><?php the_content(); ?>
	                   						 </div>
	                   						 	 <div class="price"><strong>PRICE:</strong> <span class="red">$<?php echo $custom_price;?></span>
	                   							 </div>
	                   								 <div class="price"><span><strong>COLORS:</strong></span> </br>
	                    								 <span class="colors" style="float:left;"><?php echo "<div style='height:30px; width:30px; background-color:".$custom_color."'></div>";?></span>
	                   									 <span class="colors" style="float:left;"><?php echo "<div style='height:30px; width:30px; background-color:".$custom_color_2."'></div>";?></span>
	                   									 <span class="colors"><img src="images/color3.gif" alt="" title="" border="0" /></span>                   
	                   								  </div>
	                   						<?php if(!is_user_logged_in()){?>
	                   							<a href="<?php echo get_site_url();?>/login">
	                   							<img src="<?php bloginfo('template_directory');?>/images/order_now.gif" alt="" title="" border="0" /></a>	                   						
	                   						<?php } else {?>
	                    				   		<a class="add_to_cart" id="<?php echo $id;?>" href="#">
	                   							<input type="hidden" name="base_url" id="base_url" value="<?php echo get_bloginfo('url'); ?>">
	                    						<img src="<?php bloginfo('template_directory');?>/images/order_now.gif" alt="" title="" border="0" /></a>
	                    					<?php }?>
	                    					<p id="order" style="color:red; text-align:center;"></p>
	                   						<div class="clear">
	                   						</div>
	                   				 </div>
	                    		<div class="box_bottom"></div>
	               		   </div>             
           			 	   <div class="clear"></div>
             	<?php endif;?> 
            			</div>	
                                                      
           		 <div id="demo" class="demolayout">
					<ul id="demo-nav" class="demolayout">
                		<li><a class="active" href="#tab1">More details</a></li>
               		    <li><a class="" href="#tab2">Related books</a></li>
               	    </ul>
    					 <div class="tabs-container">
            				 <div style="display: block;" class="tab" id="tab1">
                                 <p class="more_details"><?php echo $custom_details;?></p>      
                  			  </div>	
                    		  <div style="display: none;" class="tab" id="tab2">
                              		<?php 
                             		 $terms = wp_get_post_terms( $post->ID, 'pets', array("fields" => "names")); 
              						 foreach($terms as $key=>$value)
                           			 {                            	
										 $args = array(
	       											'post_type' => 'pet',
													'tax_query' => array(
																		array(
																			'taxonomy' => 'pets',
																			'field' => 'slug',
																			'terms' => $value,
																			)
																		)
														);
	       							 query_posts($args); 
                            		 if(have_posts()) :
                            			while(have_posts()) : the_post();?>
                            				<div class="new_prod_box">
                       					 		<a href="<?php the_permalink();?>"><?php the_title();?></a>
                       		 					<div class="new_prod_bg">
                       		 						<a href="<?php the_permalink();?>"><?php if(has_post_thumbnail()) : the_post_thumbnail(); endif;?></a>
                      	  						</div>                          
                  		    		        </div>
                  				 		 <?php endwhile;?>
                  				  	<?php  endif;} ?>
               					    <div class="clear"></div>
                            </div>	
						</div>
            		</div>
        			<div class="clear"></div>
        	</div><!--end of left content-->
        
      		 <?php get_sidebar();?> 
        	<div class="clear"></div>
    	</div>
       
     	<div class="clear"></div>
    	 <?php get_footer();?>
