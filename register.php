<?php
/*
Template Name: Register
*/
?>
<?php get_header();?>   
       <div class="center_content">
       		<div class="left_content">
            	<div class="title">
            		<span class="title_icon"><?php if(has_post_thumbnail()) : the_post_thumbnail(); endif;?>
            		</span>Register
            	</div>
        		<div class="feat_prod_box_details">
          		    <p class="details">
            		 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
           		    </p>
	                <div class="contact_form">
	                	<div class="form_subtitle">create new account</div>
	                    <form name="register" action="" method="post">          
	                   		 <div class="form_row">
	                    		 <label class="contact"><strong>Username:</strong></label>
	                   		     <input type="text" class="contact_input" name="user_name"/>
	                   		 </div>  
							 <div class="form_row">
	                   			  <label class="contact"><strong>Password:</strong></label>
	                  		      <input type="password" class="contact_input" name="pass_word"/>
	                   		 </div> 
		                   	 <div class="form_row">
		                  		  <label class="contact"><strong>Email:</strong></label>
		                   		  <input type="text" class="contact_input" name="email"/>
		                   	 </div>
			                 <div class="form_row">
			                      <label class="contact"><strong>Phone:</strong></label>
			                      <input type="text" class="contact_input" name="phone"/>
			                 </div>
		                   	 <div class="form_row">
		                   		 <label class="contact"><strong>Company:</strong></label>
		                    	 <input type="text" class="contact_input" name="company"/>
		                     </div>                    
	                    	 <div class="form_row">
	                    		<label class="contact"><strong>Adrres:</strong></label>
	                		    <input type="text" class="contact_input" name="address"/>
	                   		 </div>                    
	                   		 <div class="form_row">
	                       		 <div class="terms">
	                       			 <input type="checkbox" name="terms" />
	                        		  I agree to the <a href="#">terms &amp; conditions</a>                        
	                        	 </div>
	                   		 </div>                     
	                    	 <div class="form_row">
	                    		<input type="submit" class="register" value="register" />                               
	                   		 </div>   
	                   </form>     
	                </div>              
         	 	</div>	           
        		<div class="clear"></div>
        	</div><!--end of left content-->
            <?php get_sidebar();?>
       		<div class="clear"></div>
      		<?php get_footer();?>
       
  


