<div class="right_content">
	<div class="languages_box">
		<span class="red">Languages:</span>
		<a href="#" class="selected"><img src="<?php bloginfo('template_directory');?>/images/gb.gif" alt="" title="" border="0" /></a> 
		<a href="#"><img src="<?php bloginfo('template_directory');?>/images/fr.gif" alt="" title="" border="0" /></a> 
		<a href="#"><img src="<?php bloginfo('template_directory');?>/images/de.gif" alt="" title="" border="0" /></a>
	</div>
	<div class="currency">
		<span class="red">Currency: </span> 
		<a href="#">GBP</a>
		<a href="#">EUR</a> 
		<a href="#" class="selected">USD</a>
	</div>
	<div class="cart" id="main">
		<div class="title">
			<span class="title_icon">
				<img src="<?php bloginfo('template_directory');?>/images/cart.gif" alt="" title="" />
			</span>My cart
		</div>
		<div class="home_cart_content">
			<?php echo $_SESSION['count'];?> x items | <span class="red">TOTAL: <?php echo $_SESSION['total_amt'];?></span>
		</div>
		<a href="<?php echo get_bloginfo('home');?>/cart" class="view_cart">view cart</a></div>
		<div>
			<?php 
			$pet_settings = get_option( "pet_theme_settings" ); 
			if(isset($pet_settings['about_us'])):
				echo $pet_settings['about_us'];
			endif;
			?>
		</div>
		<div class="right_box">
			<div class="title">
				<span class="title_icon">
					<img src="<?php bloginfo('template_directory');?>/images/bullet4.gif" alt="" title="" />
				</span>Promotions
			</div>
			<div class="new_prod_box">
 			<?php 	    
             $args = array(
	       					'post_type' => 'pet',
							'tax_query' => array(
												array(
													'taxonomy' => 'pets',
													'field' => 'slug',
													'terms' => 'promotions'
													)
												)
							);	       
	      	 query_posts( $args ); 
	    	?>
			<?php if(have_posts()):?> 
				<?php while(have_posts()) : the_post();?>
					<div class="new_prod_box">
						<a href="<?php the_permalink();?>"><?php the_title();?></a>
						<div class="new_prod_bg">
							<span class="new_icon">
								<img src="<?php bloginfo('template_directory');?>/images/promo_icon.gif" alt="" title="" />
							</span>
							<a href="<?php the_permalink();?>">
							<?php if(has_post_thumbnail()) : the_post_thumbnail();endif;?></a>
						</div>
					</div>
				<?php endwhile;?>
			<?php endif;?></div>
		</div>
		<div class="right_box">
			<div class="title">
				<span class="title_icon">
					<img src="<?php bloginfo('template_directory');?>/images/bullet5.gif" alt="" title="" />
				</span>Categories
			</div>
			<ul class="list">
 				<?php 
				wp_list_categories(array('taxonomy'=>'pets','title_li'=>''));?>
			</ul>
			<div class="title">
				<span class="title_icon">
					<img src="<?php bloginfo('template_directory');?>/images/bullet6.gif" alt="" title="" />
				</span>Partners
			</div>
			<ul class="list">
				<li><a href="#">accesories</a></li>
				<li><a href="#">pets gifts</a></li>
				<li><a href="#">specials</a></li>
				<li><a href="#">hollidays gifts</a></li>
				<li><a href="#">accesories</a></li>
				<li><a href="#">pets gifts</a></li>
				<li><a href="#">specials</a></li>
				<li><a href="#">hollidays gifts</a></li>
				<li><a href="#">accesories</a></li>
			</ul>
		</div>
</div>
<!--end of right content-->


