<?php 
/*
Template Name: Congrats
*/
?>
<?php
get_header();
$history = congrats();
?>
 <div class="center_content">
       	<div class="left_content" id="cart">
            <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>My cart</div>
        		<div class="feat_prod_box_details">
            		<table class="cart_table">
            			<tr class="cart_title">
                			<td>Item pic</td>
                			<td>Book name</td>
                   		    <td>Unit price</td>
                   		    <td>Qty</td>
                    		<td>Total</td>  
                 		</tr>
               			 <?php foreach ( $history as $history ) 
                		 {
               				 $obj[]=json_decode($history['cartdata'],true);?>
  							 <?php foreach($obj as $key=>$value)
  							 {
   								foreach($value as $k=>$v)
   								{?>             
					            	<tr>
					                	<td><img src="<?php echo $v['image']?>" height=40px; width=40px></td>
					                	<td><?php echo $v['product_name']?></td>
					                    <td><?php echo $v['price']?></td>
					                    <td><?php echo $v['quantity']?></td>
					                    <td><?php echo $v['total']?></td>                   
					                </tr>          
								<?php }?>
							 <?php }?>
			                 <tr>
			                	<td colspan="4" class="cart_total"><span class="red">TOTAL SHIPPING:</span></td>
			                	<td> <?php echo $history['total_amt'];?></td>                
			                 </tr>  
                			 <tr>
                				<td colspan="4" class="cart_total"><span class="red">TOTAL:</span></td>
                				<td> <?php echo $history['total_amt'];?></td>                
                			 </tr>                  
     					 <?php break;}?>
            		</table>
           		 </div>	
    			 <?php 
    			 session_unset($_SESSION['product']);
    			 session_unset($_SESSION['total_amt']);
    			 ?>
        		<div class="clear"></div>
        	</div><!--end of left content-->
       		<?php get_sidebar();?>
       		<div class="clear"></div>
       	</div>
        <div class="clear"></div>
        <?php get_footer();?>

