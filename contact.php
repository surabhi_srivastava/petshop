<?php
/*
Template Name: Contact
*/
?>
<?php get_header();?>     
       
       <div class="center_content">
       		<div class="left_content">
           		 <div class="title"><span class="title_icon"><?php if(has_post_thumbnail()) : the_post_thumbnail(); endif;?></span>Contact Us</div>
        
        				<div class="feat_prod_box_details">
           					<p class="details">
            					 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
            				</p>
            				<div class="contact_form">
               					 <div class="form_subtitle">all fields are requierd
               					 </div>          
                   				 <div class="form_row">
                    				<label class="contact"><strong>Name:</strong></label>
                    				<input type="text" class="contact_input" />
                   				 </div>
                   				   
								 <div class="form_row">
                  					  <label class="contact"><strong>Email:</strong></label>
                    				  <input type="text" class="contact_input" />
                   				 </div>


			                    <div class="form_row">
			                    	<label class="contact"><strong>Phone:</strong></label>
			                    	<input type="text" class="contact_input" />
			                    </div>
                    
			                    <div class="form_row">
			                    	<label class="contact"><strong>Company:</strong></label>
			                    	<input type="text" class="contact_input" />
			                    </div>


			                    <div class="form_row">
			                    	<label class="contact"><strong>Message:</strong></label>
			                    	<textarea class="contact_textarea" ></textarea>
			                    </div>

                    
			                    <div class="form_row">
			                   		 <a href="#" class="contact">send</a>                    
			                    </div>      
               			 </div>  
            		</div>	
        			<div class="clear"></div>
        	</div><!--end of left content-->
			<div><?php get_sidebar();?></div>        
        
       
       		<div class="clear"></div>
      		<?php get_footer();?>