<?php get_header();
       //print_r($_POST);?>
       <div class="center_content">
       		<div class="left_content">
       	 		<div class="title">
       	 			<span class="title_icon">
       	 				<img src="<?php bloginfo('template_directory');?>/images/bullet1.gif" alt="" title="" />
       	 			</span>Featured pets
       	 		</div>
       			<?php 
       				$args = array(
	       					'post_type' => 'pet',
							'tax_query' => array(
												array(
													'taxonomy' => 'pets',
													'field' => 'slug',
													'terms' => 'special-pets',
													 )
												)
								  );
	       			query_posts($args); 
	    			?>
					<?php if(have_posts()):?>
						<?php while(have_posts()) : the_post();?>    							   
				        		<div class="feat_prod_box">  
				               		 <div class="prod_img" style="width:100px;"><a href="<?php the_permalink();?>"><?php if(has_post_thumbnail()) : the_post_thumbnail(); endif;?></a>
				                	 </div>                
				               		 <div class="prod_det_box">
				                		<span class="special_icon"><img src="images/special_icon.gif" alt="" title="" /></span>
				                		<div class="box_top">
				                		</div>
				                   		 <div class="box_center">
				                    			<div class="prod_title"><a href="<?php the_permalink();?>"><?php  the_title(); ?> </a>
				                    			</div>
				                   				<?php the_content('<p class="serif" style="text-align:right;"><em>' . __('-more details-') . '</em></p>');?>
				                   			   	<div class="clear">
				                   			   	</div>
				                    	</div>
				                    	<div class="box_bottom"></div>
				                    </div>   
				                    <div class="clear"></div>
				            	</div>	
            		   <?php  endwhile;?>
             	    <?php endif;?>     
            		<?php wp_reset_query(); ?>
            		<?php 	    
		               $args = array(
			       					'post_type' => 'pet',
									'tax_query' => array(
														array(
															'taxonomy' => 'pets',
															'field' => 'slug',
															'terms' => 'new-pets'
															)
														)
									);	       
	       			   query_posts( $args ); 
	    			?>
            
            
          			 <div class="title">
          			 		<span class="title_icon">
          			 			<img src="<?php bloginfo('template_directory');?>/images/bullet2.gif" alt="" title="" />
          			 		</span>New pets
          			 </div> 
           
          			 <div class="new_products">
	          			 <?php if(have_posts()):?>
								<?php while(have_posts()) : the_post();?>   
	           
					                    <div class="new_prod_box">
					                     	 <a href="<?php the_permalink();?>"><?php the_title();?></a>
					                         <div class="new_prod_bg">
					                        	 <span class="new_icon"><img src="<?php bloginfo(template_directory);?>/images/new_icon.gif" alt="" title="" /></span>
					                        	 <a href="<?php the_permalink();?>"><?php if(has_post_thumbnail()) : the_post_thumbnail();?></a>
					                         </div>           
					                    </div>
	               				<?php endif; endwhile;?>
	               		<?php endif;?>     
           			 </div> 
          			 <div class="clear"></div>
       		 </div><!--end of left content-->
    		 <?php get_sidebar();?>   
       		<div class="clear"></div>
        	<?php get_footer();?>
