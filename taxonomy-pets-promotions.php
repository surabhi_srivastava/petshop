<?php get_header();?>              
       <div class="center_content">
       		<div class="left_content">
        		<div class="crumb_nav">
           			 <a href="index.html">home</a> &gt;&gt; category name
            	</div>
            	<div class="title">
            		<span class="title_icon">
            			<img src="<?php bloginfo('template_directory');?>/images/bullet1.gif" alt="" title="" />
            		</span>Category books
            	</div>
           		<div class="new_products">
	          		 <?php 
			      		 global $query_string;
				  		 query_posts( $query_string . '&showposts=12' );				   
					 ?>
	           		 <?php if(have_posts()):?>
	          	 		<?php while(have_posts()) : the_post();?>
	                   		 <div class="new_prod_box">
	                     	 	<a href="<?php the_permalink();?>"><?php the_title();?></a>
	                       		 <div class="new_prod_bg">
	                       			  <span class="new_icon">
	                       			  		<img src="<?php bloginfo(template_directory);?>/images/promo_icon.gif" alt="" title="" />
	                       			  </span>
	                        		  <a href="<?php the_permalink();?>"><?php if(has_post_thumbnail()) : the_post_thumbnail(); endif;?></a>
	                        	</div>           
	                    	</div>                                       
	                    <?php  endwhile; ?>
	                 <?php endif;?>
               </div>      
           	   <div class="pagination">
					<?php 
					 wp_pagenavi(); 
					 ?>	
               </div>  
               <div class="clear"></div> 
        	 </div> 
          	 <?php get_sidebar();?>
       		<div class="clear"></div>
       	</div><!--end of center content-->
<?php get_footer();?>
  