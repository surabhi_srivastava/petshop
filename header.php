<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<meta name="keywords" content="<?php bloginfo('html_type'); ?>" />
<meta name="description" content="<?php bloginfo('html_type'); ?>" />
<meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=ISO-8859-6" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/lightbox.css" type="text/css" media="screen" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" /> 
<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
	<div id="wrap">
       <div class="header" style="background:url(<?php header_image();?>);height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>">
       <?php 
			$pet_settings = get_option( "pet_theme_settings" ); 
			if(isset($pet_settings['logo'])):
				$image=$pet_settings['logo'];
				?>
       		<div class="logo"><a href="<?php echo get_option('home'); ?>/"><img src="<?php echo bloginfo('template_directory').$image;?>"></a></div>         
			<?php endif;?>	
			<?php wp_nav_menu( 
					array( 
						'theme_location' => '',
						'menu' => 'petshop', 
						'container_id' => 'menu', 
						'container' => 'div', 
						'container_class' => '', 
						'walker' => new description_walker()) );?>
       </div> 
      
