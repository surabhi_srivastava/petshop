<?php 
add_action( 'init', 'pet_admin_init' );
add_action( 'admin_menu', 'pet_settings_page_init' );

function pet_admin_init() 
{
	$settings = get_option( "pet_theme_settings" );
	if ( empty( $settings ) ) {
		$settings = array(
			'logo' => false,
			'about_us' => 'Some intro text about yourself'
		);
		add_option( "pet_theme_settings", $settings, '', 'yes' );
	}	
}

function pet_settings_page_init() 
{
	$theme_data = get_theme_data( PET_THEME_DIR. '/style.css' );
	$settings_page = add_theme_page( $theme_data['Name']. ' Theme Settings', $theme_data['Name']. ' Theme Settings', 'edit_theme_options', 'theme-settings', 'pet_settings_page' );
	add_action( "load-{$settings_page}", 'pet_load_settings_page' );
}

function pet_load_settings_page() 
{
	if ( $_POST["pet-settings-submit"] == 'Y' ) 
	{
		check_admin_referer( "pet-settings-page" );
		pet_save_theme_settings();
		$url_parameters = isset($_GET['tab'])? 'updated=true&tab='.$_GET['tab'] : 'updated=true';
		wp_redirect(admin_url('themes.php?page=theme-settings&'.$url_parameters));
		exit;
	}
}

function pet_save_theme_settings() 
{
	global $pagenow;
	$settings = get_option( "pet_theme_settings" );
	
	if ( $pagenow == 'themes.php' && $_GET['page'] == 'theme-settings' )
	{ 
		if ( isset ( $_GET['tab'] ) )
	        $tab = $_GET['tab']; 
	    else
	        $tab = 'logo'; 

	    switch ( $tab )
	    { 
	        case 'about_us' :
				$settings['about_us']	  =  stripslashes($_POST['about_us']);
			break; 
			case 'logo' : 				
				if($_FILES['file']['name'])
				{
					$target='/upload/'.$_FILES['file']['name'];
					move_uploaded_file($_FILES['file']['tmp_name'], PET_THEME_DIR.'/upload/'.$_FILES['file']['name']);			
					$settings['logo'] = $target;
				}				
			break;
	    }
		if(!current_user_can( 'unfiltered_html' ) )
		{
			if ( $settings['about_us'] )
			$settings['about_us'] = stripslashes( esc_textarea( wp_filter_post_kses( $settings['about_us'] ) ) );
			
		}
	}
	$updated = update_option( "pet_theme_settings", $settings );
}

function pet_admin_tabs( $current = 'logo' ) 
{ 
    $tabs = array( 'logo' => 'Change Logo', 'about_us' => 'About Us'); 
    // $links = array();
    echo '<div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
        
    foreach( $tabs as $tab => $name )
    {
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?page=theme-settings&tab=$tab'>$name</a>";        
    }
    echo '</h2>';
}

function pet_settings_page() 
{
	global $pagenow;
	$settings = get_option( "pet_theme_settings" );
	$theme_data = get_theme_data( PET_THEME_DIR . '/style.css' );
	?>
	
	<div class="wrap">
		<h2><?php echo $theme_data['Name']; ?> Theme Settings</h2>
		
		<?php
			if ( 'true' == esc_attr( $_GET['updated'] ) ) echo '<div class="updated" ><p>Theme Settings updated.</p></div>';
			
			if ( isset ( $_GET['tab'] ) ) pet_admin_tabs($_GET['tab']); else pet_admin_tabs('logo');
		?>

		<div id="poststuff">
			<form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>" enctype="multipart/form-data">
				<?php
				wp_nonce_field( "pet-settings-page" ); 
				
				if ( $pagenow == 'themes.php' && $_GET['page'] == 'theme-settings' )
				{ 				
					if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab']; 
					else $tab = 'logo'; 
					
					echo '<table class="form-table">';
					switch ( $tab )
					{
						case 'about_us' : 
							?>
							<tr>
								<td>
									<?php 									
									wp_editor($settings["about_us"],'about_us'); ?>
									<span class="description">Enter something about yourself:</span>
								</td>
							</tr>
							<?php
						break;
						case 'logo' : 
							?>
							<tr>
								<th><label for="logo"><strong>Main Logo</strong></label>
								<img src="<?php echo bloginfo('template_directory').$settings['logo'];?>"></tr>
								<tr>
								<td colspan=3>
									<input type='file' name='file' id='file'><br/>
									<span class="description">Enter the introductory text for the home page:</span>
								</td>
							</tr>
							<?php
						break;
					}
					echo '</table>';
				}
				?>
				<p class="submit" style="clear: both;">
					<input type="submit" name="Submit"  class="button-primary" value="Update Settings" />
					<input type="hidden" name="pet-settings-submit" value="Y" />
				</p>
			</form>
			
			<p><?php echo $theme_data['Name'] ?></p>
		</div>

	</div>
<?php
}


