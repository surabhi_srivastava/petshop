<?php 

add_action( 'init', 'create_post_type' );
add_action( 'init', 'create_taxonomy' );

/**
 * Used to create custom post type pet
 */
function create_post_type() {
  register_post_type( 'pet',
    array(
      'labels' => array(
        'name' => __( 'Pets_post' ),
        'singular_name' => __( 'Pet_post' )
      ),
	  'capability_type' =>  'post',
      'public' => true,
	  'supports' => array(
	  'title',
	  'editor',
	  'excerpt',
	  'trackbacks',
	  'custom-fields',
	  'revisions',
	  'thumbnail',
	  'author',
	  )
    )
  );
}

function create_taxonomy() {
	// create a new taxonomy
	register_taxonomy(
		'pets',
		'pet',
		array(
			'label' => __( 'Pets' ),
		//	'rewrite' => array( 'slug' => 'person' ),
			'hierarchical'  => true,
		)
		
	);
}
