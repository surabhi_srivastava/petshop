<?php
add_action( 'add_meta_boxes', 'myplugin_add_custom_box' );

// backwards compatible (before WP 3.0)

/* Do something with the data entered */
add_action( 'save_post', 'myplugin_save_postdata' );

/* Adds a box to the main column on the Post and Page edit screens */
function myplugin_add_custom_box() 
{
	$screens = array( 'post', 'page','pet' );
	foreach ($screens as $screen) 
	{
		add_meta_box(
           			 'myplugin_sectionid',
					 __( 'My Post Section Title', 'myplugin_textdomain' ),
            		 'myplugin_inner_custom_box',
					 $screen					
					 );						
	}
}

/* Prints the box content */
function myplugin_inner_custom_box( $post ) 
{
	// Use nonce for verification
	wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_noncename' );
	
	// The actual fields for data entry
	// Use get_post_meta to retrieve an existing value from the database and use the value for the form
	
	$details = get_post_meta( $post->ID, 'pet_desc', true );
	wp_editor($details,'pet_desc'); 
	echo "<table><tr>";

	
	$discount_value = get_post_meta( $post->ID, 'discount', true );
	echo '<tr><td><label for="discount_value">';
	_e("Discount", 'myplugin_textdomain' );
	echo '</label> </td>';
	echo '<td><input type="text" id="discount_value" name="discount_value" value="'.esc_attr($discount_value).'" size="25" /></td></tr>';

	
	$promotion_type = get_post_meta( $post->ID, 'promotion', true );
	echo '<tr><td><label for="promotion_type">';
	_e("Promotion", 'myplugin_textdomain' );
	echo '</label></td> ';
	echo '<td><input type="text" id="promotion_type" name="promotion_type" value="'.esc_attr($promotion_type).'" size="25" /></td></tr>';

	
	$price = get_post_meta( $post->ID, 'price', true );
	echo '<tr><td><label for="price">';
	_e("Price", 'myplugin_textdomain' );
	echo '</label> </td>';
	echo '<td><input type="text" id="price" name="price" value="'.esc_attr($price).'" size="25" /></td></tr>';

	
	echo '<tr><td><label for="meta-color" class="example-row-title">Color Picker-1</label>';
    echo '<td><input name="meta-color" type="text" value="'.$example_stored_meta['meta-color'][0].'" class="meta-color" /></td>';
    
    
    echo '<td><label for="color-2" class="example-row-title">Color Picker-2</label></td>';
    echo '<td><input name="color-2" type="text" value="'.$example_stored_meta['color-2'][0].'" class="meta-color" /></td></tr></table>';
 
}

/* When the post is saved, saves our custom data */
function myplugin_save_postdata( $post_id ) 
{
	// First we need to check if the current user is authorised to do this action.
	if ( 'page' == $_POST['post_type'] ) 
	{
		if ( ! current_user_can( 'edit_page', $post_id ) )
		return;
	} 
		else 
		{
			if ( ! current_user_can( 'edit_post', $post_id ) )
			return;
		}

	// Secondly we need to check if the user intended to change this value.
	if ( ! isset( $_POST['myplugin_noncename'] ) || ! wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename( __FILE__ ) ) )
		return;

	// Thirdly we can save the value to the database

	//if saving in a custom table, get post_ID
	$post_ID = $_POST['post_ID'];

	//sanitize user input
	$mydata_details = stripslashes($_POST['pet_desc']);
	$mydata_discount = sanitize_text_field( $_POST['discount'] );
	$mydata_promotion = sanitize_text_field( $_POST['promotion'] );
	$mydata_price = sanitize_text_field( $_POST['price'] );
	$mydata_color= sanitize_text_field( $_POST['meta-color'] );
	
	// Do something with $mydata
	// either using
	add_post_meta($post_ID, 'pet_desc', $mydata_details, true) or
	update_post_meta($post_ID, 'pet_desc', $mydata_details);
	// or a custom table (see Further Reading section below)
	
	add_post_meta($post_ID, 'discount', $mydata_discount, true) or
	update_post_meta($post_ID, 'discount', $mydata_discount);
	// or a custom table (see Further Reading section below)
	
	
	add_post_meta($post_ID, 'promotion', $mydata_promotion, true) or
	update_post_meta($post_ID, 'promotion', $mydata_promotion);
	
	
	add_post_meta($post_ID, 'price', $mydata_price, true) or
	update_post_meta($post_ID, 'price', $mydata_price);
	
	
	if( isset( $_POST[ 'meta-color' ] ) )
	{
		add_post_meta($post_ID, 'meta-color', $mydata_color, true) or
	    update_post_meta( $post_id, 'meta-color', $_POST[ 'meta-color' ] );
	}
	
	if( isset( $_POST[ 'color-2' ] ) ) 
	{
		add_post_meta($post_ID, 'color-2', $mydata_color, true) or
	    update_post_meta( $post_id, 'color-2', $_POST[ 'color-2' ] );
	}
}