<?php
/**
 * For Logout
 */
add_filter('wp_nav_menu_items', 'logout_link', 10, 2);
function logout_link($items, $args) {
	if(is_user_logged_in())
	{
		$items .= "<li><a href=".wp_logout_url( home_url() )."><b>Logout</b></a></li>";
		//return $items;
	} 
		return $items;
}