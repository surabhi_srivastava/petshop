<?php
/**
 * To register new users
 */
add_action('init','register');
function register()
{
	if(isset($_POST['user_name']) && isset($_POST['pass_word']))
	{
		$name=$_POST['user_name'];
		$pass=$_POST['pass_word'];
		$email=$_POST['email'];
		if($_POST['email'] && $_POST['company'] && $_POST['address'])
		{
			$user_id = username_exists( $name );
			if(isset($_POST['terms']))
			{
				if ( !$user_id and email_exists($email) == false ) 
				{
					$user_id = wp_create_user( $name, $pass, $email ); ?>
					<?php $url=get_bloginfo('home')."/my-account"; ?>
					<script>
					var url = "<?php echo $url; ?>";
					window.location = url;
					</script>
					<?php 
				} 
				else 
				{
					echo "<script type='text/javascript'> alert('Username already registered'); </script>";
				}
			}
			else echo "<script type='text/javascript'> alert('Please agree to terms and conditions'); </script>";
		}
	}
}