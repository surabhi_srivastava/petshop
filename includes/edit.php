<?php
session_start();
if(isset($_GET['id']))
{
	$id=$_GET['id'];
}
if(isset($_GET['action']))
{
	$act=$_GET['action'];
}
$flag=0;
if($act=='add')
{
	$_SESSION['count']=0;
	if(!isset($_SESSION['total']))
	{
		$_SESSION['total_amt']=0;		
	}
	$custom_details = get_post_meta($id, 'pet_desc', true);
	$custom_price = get_post_meta($id,'price',true);
	$name = get_the_title($id);
	$url = wp_get_attachment_url( get_post_thumbnail_id($id) );
	if(isset($_SESSION['product']))
	{
		foreach($_SESSION['product'] as $key=>$value)
		{
			if($_SESSION["product"][$key]['id']==$id)
			{
				$flag=1;
			}
		}
	}
	if($flag==0)
	{ 
		$_SESSION['product'][] = array('id'=>$id, 'image'=>$url, 'product_name'=>$name,'price'=>$custom_price,'quantity'=>0,'total'=>0);
	}
	
	foreach($_SESSION['product'] as $key=>$value)
	{
		if($_SESSION['product'][$key]['id']==$id)
		{
			$_SESSION['product'][$key]['quantity']+=1;
			$_SESSION['product'][$key]['total']=$_SESSION['product'][$key]['price']*$_SESSION['product'][$key]['quantity'];				
		}
		$_SESSION['total_amt']+=$_SESSION['product'][$key]['total'];
		$_SESSION['count']+=$_SESSION['product'][$key]['quantity'];
	}
	$data = json_encode(array('error'=>0, 'count'=>$_SESSION['count'], 'total_amt'=>$_SESSION['total_amt']));
	echo $data; 
	exit;
}
if($act=='delete')
{
	foreach($_SESSION['product'] as $key=>$value)
	{
		if($_SESSION["product"][$key]['id']==$id)
		{
			$_SESSION['total_amt']-=$_SESSION['product'][$key]['total'];
			$_SESSION['count']-=$_SESSION['product'][$key]['quantity'];
			unset($_SESSION["product"][$key]);				
		}			
	}
	?>
		
	            <tr class="cart_title">
	                <td>Item pic</td>
	                <td>Book name</td>
	                <td>Unit price</td>
	                <td>Qty</td>
	                <td>Total</td>  
	                <td>Delete Product</td>
	            </tr>
	   <?php foreach($_SESSION['product'] as $key=>$value)
	   {
	   		$id=$_SESSION['product'][$key]['id'];
	   		$uid=$_SESSION['product'][$key]['id'];
	   ?>             
	            <tr>
	                <td><img src="<?php echo $_SESSION['product'][$key]['image']?>" height=40px; width=40px></td>
	                <td><?php echo $_SESSION['product'][$key]['product_name']?></td>
	                <td><?php echo $_SESSION['product'][$key]['price']?></td>
	                <td><input type="text" class="quant" value="<?php echo $_SESSION['product'][$key]['quantity']?>" id="<?php echo $uid;?>" size=5></td>
	                <td><?php echo $_SESSION['product'][$key]['total']?></td>   
	                <td><button id="<?php echo $id;?>" class="del">Delete</button></td>	                       	                            
	           </tr>          
	   <?php }?>
	            <tr>
	                <td colspan="4" class="cart_total"><span class="red">TOTAL SHIPPING:</span></td>
	                <td> <?php echo $_SESSION['total_amt'];?></td>                
	           </tr>  
	                
	           <tr>
	                <td colspan="4" class="cart_total"><span class="red">TOTAL:</span></td>
	                <td> <?php echo $_SESSION['total_amt'];?></td>                
	           </tr>                  
	            	          
		        <input type="hidden" id="count" value="<?php echo $_SESSION['count'];?>">
		        <input type="hidden" id="total_amt" value="<?php echo $_SESSION['total_amt'];?>">
		        <input type="hidden" id="url" value="<?php echo get_bloginfo('home');?>">
<?php 
}
if($act=='update')
{
?>
	
	            <tr class="cart_title">
	                <td>Item pic</td>
	                <td>Book name</td>
	                <td>Unit price</td>
	                <td>Qty</td>
	                <td>Total</td>  
	                <td>Delete Product</td>	                       
	            </tr>
	            <?php 
				 $_SESSION['total_amt']=0;
	  			 $_SESSION['count']=0;
				 foreach($_SESSION['product'] as $key=>$value)
				 {
					if($_SESSION['product'][$key]['id']==$id)
					{
						$_SESSION['product'][$key]['quantity']=$_GET['quan'];
						$_SESSION['product'][$key]['total']=$_SESSION['product'][$key]['price']*$_SESSION['product'][$key]['quantity'];	
					}
					$_SESSION['total_amt']+=$_SESSION['product'][$key]['total'];
					$_SESSION['count']+=$_SESSION['product'][$key]['quantity'];
					$i=$_SESSION['product'][$key]['id'];
					$unique=$_SESSION['product'][$key]['id'];
				?>	           
		            <tr>
		                <td><img src="<?php echo $_SESSION['product'][$key]['image']?>" height=40px; width=40px></td>
		                <td><?php echo $_SESSION['product'][$key]['product_name']?></td>
		                <td><?php echo $_SESSION['product'][$key]['price']?></td>
		                <td><input type="text" class="quant" value="<?php echo $_SESSION['product'][$key]['quantity']?>" id="<?php echo $unique;?>" size=5></td>
		                <td><?php echo $_SESSION['product'][$key]['total']?></td>   
		                <td><button id="<?php echo $i;?>" class="del">Delete</button></td>
		            </tr>          
				<?php }?>
		            <tr>
		                <td colspan="4" class="cart_total"><span class="red">TOTAL SHIPPING:</span></td>
		                <td> <?php echo $_SESSION['total_amt'];?></td>                
		            </tr>  
	                
		            <tr>
		                <td colspan="4" class="cart_total"><span class="red">TOTAL:</span></td>
		                <td> <?php echo $_SESSION['total_amt'];?></td>                
		            </tr>                  
	            
	      		   <input type="hidden" id="counter" value="<?php echo $_SESSION['count'];?>">
	      		   <input type="hidden" id="total" value="<?php echo $_SESSION['total_amt'];?>">	        
<?php         
}
if($act == 'checkout')
{
	global $user_ID, $user_identity; get_currentuserinfo();
	$id=$user_ID;
	$item=array();
	foreach($_SESSION['product'] as $key=>$value)
	{
		if($value['quantity']!=0)
		{
			           $item[]=array("id"=>$value['id'],
		                            "image"=>$value['image'],
		                             "product_name"=>$value['product_name'],
		                              "price"=>$value['price'],
		                              "quantity"=>$value['quantity'],
		                               "total"=>$value['total']);
		}
	}
	$cart_data=json_encode($item);
	$id=$user_ID;
	$total=$_SESSION['total_amt'];
	$date= date("y/m/d : H:i:s", time());
	$cart=array('userid' => $id,
				'cartdata' => $cart_data,
				'total_amt' => $total,
				'date' => $date);
	global $wpdb;	
	$result = $wpdb->insert('cartdata',$cart);	
}
