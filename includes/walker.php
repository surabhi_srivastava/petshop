<?php

/**
 * wp nav menu walker class
 */
class description_walker extends Walker_Nav_Menu
{
      function start_el(&$output, $item, $depth, $args)
      {
           global $wp_query;
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
			
    	   if($item->ID == 205 && !is_user_logged_in()){
           	return;
           }
           if($item->ID == 223 && is_user_logged_in())
           {
           	return;
           }
           if($item->ID == 220 && is_user_logged_in())
           {
           	return;
           }
           $class_names = $value = '';
           
           //print_r($item);

           $classes = empty( $item->classes ) ? array() : (array) $item->classes;

           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
           $class_names = ' class="'. esc_attr( $class_names ) . '"';
           
           //if($item->ID != 197){
           	$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
          //}
           $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
           $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
           $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
           $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

           $prepend = '<strong>';
           $append = '</strong>';
           //$description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

           if($depth != 0)
           {
                     $description = $append = $prepend = "";
           }

            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
            $item_output .= $description.$args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;
            
            //if($item->ID != 197){
            	$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
            //}
            
        }
}

