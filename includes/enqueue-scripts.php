<?php 

	
	add_action('wp_enqueue_scripts', 'pet_enqueue_scripts');
	
	/**
	 * To enqueue scripts 
	 */
	function pet_enqueue_scripts(){
		wp_enqueue_script( 'prototype' );
		wp_enqueue_script( 'scriptaculous' );
		wp_enqueue_script( 'scriptaculous-effects' );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'lightbox', PET_THEME_URL . '/js/lightbox.js' , __FILE__ );
	//	wp_enqueue_script( 'java', PET_THEME_URL . '/js/java.js' , __FILE__ );
		wp_enqueue_script( 'script', PET_THEME_URL . '/js/script.js' , __FILE__ , true, true );
		wp_enqueue_script( 'shop', PET_THEME_URL . '/js/shop.js' , __FILE__);
	}
	
	/**
	 * Loads the color picker javascript
 	*/
	function pet_admin_color_enqueue()
	 {
   		 global $typenow;
   		 if( $typenow == 'pet' )
   		 {
        	 wp_enqueue_style( 'wp-color-picker' );
        	 wp_enqueue_script('jquery');
        	 wp_enqueue_script( 'meta-color-js', get_stylesheet_directory_uri() . '/js/meta-color.js', array( 'wp-color-picker' ),true,true);
  		  }
	} // End example_color_enqueue()
	add_action( 'admin_enqueue_scripts', 'pet_admin_color_enqueue' );	
	
	