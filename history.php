<?php
/*
Template Name: History
*/
?>
<?php 
get_header();
$history=old_history();
?>
<div class="center_content">
       	<div class="left_content">
            <div class="title"><span class="title_icon"><?php if(has_post_thumbnail()) : the_post_thumbnail(); endif;?></span>My account
            </div>
        
        	<div class="feat_prod_box_details">
        		<table>
        			<?php 
        			foreach ( $history as $history ) 
					{
						$obj[]=json_decode($history['cartdata'],true);
						echo "<tr><td><b>Product ID</b></td><td>".$history['id']."</td></tr>";
						echo "<td><b>Image</b></td><td><b>Product</b></td><td><b>Price</b></td><td><b>Quantity</b></td><td><b>Total</b></td>";
						foreach($obj as $key=>$value)
						{
							foreach($value as $a=>$b)
							{
								echo "<tr><td><img src=".$b['image']." height=40px width=40px></td><td>".$b['product_name']."</td><td>".$b['price']."</td><td>".$b['quantity']."</td><td>".$b['total']."</td></tr>";
							}
							unset($obj);
						}
						echo "<tr><td><b>Total - </b>".$history['total_amt']."</td><td colspan=2><b>Date - </b>".$history['date']."</td></tr>";
					}
					?>
			   </table>
        	</div>
        	
            
        	<div class="clear"></div>
       </div><!--end of left content-->
	   <?php get_sidebar();?>       
       
       <div class="clear"></div>
</div><!--end of center content-->
<?php get_footer(); ?>
        	