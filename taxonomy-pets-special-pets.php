<?php 
get_header(); 
?>                               
       <div class="center_content">
       		<div class="left_content">
	       		<?php 
			       global $query_string;
				   query_posts( $query_string . '&showposts=10' );	
				?>	 			        
				<?php if(have_posts()):?>
					<?php while(have_posts()) : the_post();?>       
		        		<div class="feat_prod_box">            
		            		<div class="prod_img" style="width:100px;">
		            			<a href="<?php the_permalink();?>">
		            				<?php if(has_post_thumbnail()) : the_post_thumbnail(); endif;?>
		            			</a>
		            		</div>                
		               	    <div class="prod_det_box">
		                		<span class="special_icon">
		                			<img src="images/special_icon.gif" alt="" title="" />
		                		</span>
		                		<div class="box_top"></div>
		                    	<div class="box_center">                    
		                    		<div class="prod_title"><a href="<?php the_permalink();?>"><?php  the_title(); ?> </a>
		                    		</div>
		                    		<?php the_content('<p class="serif" style="text-align:right;"><em>' . __('-more details-') . '</em></p>');?>
		                    		<div class="clear"></div>
		                    	</div>
		                    	<div class="box_bottom"></div>              		                    
		                	</div>   		                                  
		            		<div class="clear"></div>
		            	</div>
		          <?php endwhile;?>
             <?php endif;?>
             <div class="navigation">
				<?php 
				wp_pagenavi(); 
				?>
			 </div>             
           </div><!--end of left content-->        
		   <?php get_sidebar();?>
       	   <div class="clear"></div>              
      </div> 
<?php get_footer();?>
      