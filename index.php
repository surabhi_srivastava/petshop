<?php get_header();?>
	<div class="center_content">
    	<div class="left_content">
       		<?php if(have_posts()):?>
       			<?php while(have_posts()) : the_post();?>
       				<div class="title"><?php the_title();?></div>
       				<div style="clear:both;"><?php the_content();?></div>
       			<?php endwhile; ?>
       		<?php endif;?>
        	<div class="clear"></div>
        </div><!--end of left content-->
        <?php get_sidebar();?>
		<div class="clear"></div>
    </div><!--end of center content-->
<?php get_footer();?>