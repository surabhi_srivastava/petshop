<?php
/**
 * Template Name: Cart
 */
?>
<?php get_header();?>
 <div class="center_content">
       	<div class="left_content" id="cart">
            <div class="title"><span class="title_icon"><img src="images/bullet1.gif" alt="" title="" /></span>My cart</div>
        		<div class="feat_prod_box_details">
            		<table class="cart_table">
		            	<tr class="cart_title">
		                	<td>Item pic</td>
		                	<td>Book name</td>
		                    <td>Unit price</td>
		                    <td>Qty</td>
		                    <td>Total</td>  
		                    <td>Delete Product</td>
		                </tr>
                		<?php if(isset($_SESSION['product'])){?>
						    <?php foreach($_SESSION['product'] as $key=>$value)
						    {
						   		$id=$_SESSION['product'][$key]['id'];
						   		$uid=$_SESSION['product'][$key]['id'];
						  	 ?>             
			            	<tr>
			                	<td><img src="<?php echo $_SESSION['product'][$key]['image']?>" height=40px; width=40px></td>
			                	<td><?php echo $_SESSION['product'][$key]['product_name']?></td>
			                    <td><?php echo $_SESSION['product'][$key]['price']?></td>
			                    <td><input type="text" class="quant" value="<?php echo $_SESSION['product'][$key]['quantity']?>" id="<?php echo $uid;?>" size=5></td>
			                    <td><?php echo $_SESSION['product'][$key]['total']?></td>   
			                    <td><button id="<?php echo $id;?>" class="del">Delete</button></td>                            
			                </tr>          
							<?php }?>
			                <tr>
			                	<td colspan="4" class="cart_total"><span class="red">TOTAL SHIPPING:</span></td>
			                	<td> <?php echo $_SESSION['total_amt'];?></td>                
			                </tr>  
			                
			                <tr>
			               	   <td colspan="4" class="cart_total"><span class="red">TOTAL:</span></td>
			               	   <td> <?php echo $_SESSION['total_amt'];?></td>                
			                </tr>                  
            			<?php }?>
           			 </table>
            	<input type="hidden" name="url" id="url" value="<?php echo get_bloginfo('url'); ?>">       		
            	<a href="#" class="continue">&lt; continue</a>
            	<?php global $user_ID, $user_identity; get_currentuserinfo(); 
            	if(!$user_ID){?>
            		<a href="<?php echo get_site_url();?>/login" class="checkout" id="notin">checkout</a>
           		 <?php } else { ?>
           			 <input type="hidden" name="base" id="base" value="<?php echo get_bloginfo('url'); ?>">       		
           			 <a href="#" class="checkout" id="include">checkout &gt;</a>
       		     <?php }?>
       		 </div>	
     
       		 <div class="clear"></div>
        </div><!--end of left content-->
        <?php get_sidebar();?>
        <div class="clear"></div>
     </div>
       
     <div class="clear"></div>
     <?php get_footer();?>
