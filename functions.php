<?php
add_action('init','my_session');
function my_session()
{
	if(!session_id()){
		session_start();
	}
}

define('PET_THEME_DIR', get_template_directory());
define('PET_THEME_URL', get_template_directory_uri());

/**
 * Used to support theme functions
 */
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails', array( 'page','post','pet' ) ); 

/**
 * Used to register sidebar
 */
if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));

	
/**
 * Included files
 */
include( PET_THEME_DIR . '/includes/meta-boxes.php');
include( PET_THEME_DIR . '/includes/custom-post-type.php');
include( PET_THEME_DIR . '/includes/enqueue-scripts.php');
include( PET_THEME_DIR . '/includes/themesettings.php');
include( PET_THEME_DIR . '/includes/walker.php');
include( PET_THEME_DIR . '/includes/theme-header.php');
include( PET_THEME_DIR . '/includes/logout.php');
include( PET_THEME_DIR . '/includes/congo.php');
include( PET_THEME_DIR . '/includes/edit.php');
include( PET_THEME_DIR . '/includes/old-history.php');
include( PET_THEME_DIR . '/includes/registration.php');


/**
 * Used to get parameters of jquery
 */
add_filter('query_vars', 'parameter_queryvars' );

function parameter_queryvars( $qvars )
{
	$qvars[] = ' action';
	$qvars[] = ' id';
	return $qvars;
}

