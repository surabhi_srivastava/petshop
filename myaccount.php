<?php
/*
Template Name: Myaccount
*/
?>
<?php global $user_ID, $user_identity; get_currentuserinfo(); if (!$user_ID) { ?>   
<?php get_header();?>     
       <div class="center_content">
       		<div class="left_content">
            	<div class="title"><span class="title_icon"><?php if(has_post_thumbnail()) : the_post_thumbnail(); endif;?></span>My account
            	</div>
        		<div class="feat_prod_box_details">
           			 <p class="details">
            			 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.
           			 </p>
              		 <div class="contact_form">
               			 <div class="form_subtitle">login into your account</div>
                 			<form name="register" action="<?php bloginfo('url') ?>/wp-login.php" method="post" class="wp-user-form">          
                    			<div class="form_row">
                   					 <label class="contact"><strong>Username:</strong></label>
                   					 <input type="text" name="log" value="<?php echo esc_attr(stripslashes($user_login)); ?>" size="20" id="user_login" tabindex="11" />
                   				 </div>  
								 <div class="form_row">
                    				<label class="contact"><strong>Password:</strong></label>
									<input type="password" name="pwd" value="" size="20" id="user_pass" tabindex="12" />                   
									</div>                     
								<div class="form_row">
                       				 <div class="terms">
                       					 <label for="rememberme">
										 <input type="checkbox" name="rememberme" value="forever" checked="checked" id="rememberme" tabindex="13" /> Remember me
										</label>
                       				 </div>
                   				</div> 
								<div class="form_row">      
				                     <?php do_action('login_form'); ?>       
									 <input type="submit" name="user-submit" value="<?php _e('Login'); ?>" tabindex="14"  />
									 <input type="hidden" name="redirect_to" value="<?php bloginfo('home'); ?>" />
									 <input type="hidden" name="user-cookie" value="1" />
			                   </div>
                  			</form>     
                     </div>  
           		</div>	               
       		    <div class="clear"></div>
       		 </div><!--end of left content-->
        	 <?php get_sidebar();?>       
       		<div class="clear"></div>
       </div><!--end of center content-->
       <?php get_footer(); }?>